TABLE_DIR = THLiu.Tables
TABLE_SRC = THLiu.txt

TABLE_TABLE_SRC = \
	00_header.txt \
	$(TABLE_DIR)/00\ Bopomofo.txt \
	$(TABLE_DIR)/01\ CJK\ Unified\ Ideographs.txt \
	$(TABLE_DIR)/02\ CJK\ Unified\ Ideographs\ Extension\ A.txt \
	$(TABLE_DIR)/03\ CJK\ Unified\ Ideographs\ Extension\ B.txt \
	$(TABLE_DIR)/04\ CJK\ Unified\ Ideographs\ Extension\ C.txt \
	$(TABLE_DIR)/05\ CJK\ Unified\ Ideographs\ Extension\ D.txt \
	$(TABLE_DIR)/13\ CJK\ Compatibility\ Forms.txt \
	$(TABLE_DIR)/14\ Halfwidth\ and\ Fullwidth\ Forms.txt \
	$(TABLE_DIR)/30\ CJK\ Symbols\ and\ Punctuation.txt \
	Hangul-Pinyin.txt

FCITX_TABLE_DIR = usr/share/fcitx/table
FCITX_LOCAL_TABLE_DIR = $(HOME)/.config/fcitx/table

all: boshiamy.mb

init: decompose.py $(TABLE_SRC)
	python3 decompose.py $(TABLE_SRC) $(TABLE_DIR)

boshiamy.txt : $(TABLE_TABLE_SRC)
	cat  $(TABLE_TABLE_SRC) > $@

boshiamy.mb : boshiamy.txt
	txt2mb $< $@

install : $(FCITX_LOCAL_TABLE_DIR)/boshiamy.mb
	fcitx -r

$(FCITX_LOCAL_TABLE_DIR)/boshiamy.mb: boshiamy.mb
	-mkdir -p $(FCITX_LOCAL_TABLE_DIR)
	cp $< $@

.PHONY: all init install
