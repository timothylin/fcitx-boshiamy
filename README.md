# A revised FCITX's Boshiamy （嘸蝦米） table from [蝦米族樂園](http://liu.twbbs.org)


# Build and Install
* To extract tables based on Unicode blocks.<br>
    `make init`
* To rebuild a new Boshiamy table.<br>
    `$ make all`
* To install the new Boshiamy table to the local folder.<br>
    `$ make install`


# Tables of Unicode Blocks
## Included unicode blocks
### ㄅㄆㄇㄈ注音字母，CJK 統一表意文字 丶擴展 A/B/C/D 區，CJK 相容形式， CJK 符號和標點，半型及全型
- Bopomofo of range [3100..312F]
- CJK Unified Ideographs of range [4E00..9FFF]
- CJK Unified Ideographs Extension A of range [3400..4DBF]
- CJK Unified Ideographs Extension B of range [20000..2A6DF]
- CJK Unified Ideographs Extension C of range [2A700..2B73F]
- CJK Unified Ideographs Extension D of range [2B740..2B81F]
- CJK Compatibility Forms of range [FE30..FE4F]
- CJK Symbols and Punctuation [3000...303F]
- Halfwidth and Fullwidth Forms of range [FF00..FFEE]


## Explicitly excluded Unicode blocks
### CJK 相容表意文字， CJK 圍繞字元及月份，CJK 相容字元，漢文訓読, 平仮名，片仮名
- CJK Compatibility Ideographs of range [F900..FAFF]
- Enclosed CJK Letters and Months of range [3200..32FF]
- CJK Compatibility of range [3300..33FF]
- Kanbun of range [3190..319F]
- Hiragana of range [3040..309F]
- Katakana of range [30A0..30FF]


# Reference.
## The original open-source Boshiamy table. <br>
- [THLiu.zip](https://drive.google.com/drive/folders/0B_9ob1iJjpkLMjA3MDYzM2YtMWNhOS00MTUxLTlhMGUtYmM3YjJlYjU2ODE5?hl=en)


## Hangul-Pinyin table source. <br>
- [繁簡中、日、韓合體！利用嘸蝦米輸入韓文](https://evanlab.blogspot.com/2017/06/blog-post_23.html)
- [韓文字碼表](https://drive.google.com/file/d/0B7cMgt1ql7HmS2JnemdTM1FEUXc/view)


## Optional installation/setup steps on various Linux distributions:
- Manually install fcitx packages:
    - **fcitx**
    - **fcitx-bin**
    - **fcitx-config-qt{3,4,5}** or **fcitx-config-gtk** or **kde-config-fcitx**
    - **fcitx-tools**
    - **fcitx-table-boshiamy** or **fcitx-table-extra**
- Manually assign the **input method engine**: `im-config -n fcitx`
    - use `im-config -l` to ensure that fcitx has been correctly installed.
- Manually add the **input method table** 
    - Right-click the **keyboard icon** on the status bar
    - Select **Configure Current Input Method**
    - Select **+**
    - Un-check **Only Show Current Language**, then select **Boshiamy**
    - Use **>** button to add **Boshiamy** to "Current Input Method" box.
    - Click **OK** button
- Log-out and then log-in to ensure that changed IM settings are  correctly updated and loaded.
