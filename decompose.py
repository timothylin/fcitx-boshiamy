#!/usr/bin/python3
#
# Decompose a Boshiamy table to unicode-block-based files.
#
# Ref.
#   Unicode:
#       https://en.wikibooks.org/wiki/Unicode/Character_reference
#   Hangul:
#       https://evanlab.blogspot.com/2017/06/blog-post_23.html
#       https://drive.google.com/file/d/0B7cMgt1ql7HmS2JnemdTM1FEUXc/view?usp=sharing
#       https://zh.wikipedia.org/wiki/%E6%9C%9D%E9%AE%AE%E8%AA%9E%E6%8B%89%E4%B8%81%E5%8C%96
#   Boshiamy Table:
#       https://drive.google.com/drive/folders/0B_9ob1iJjpkLMjA3MDYzM2YtMWNhOS00MTUxLTlhMGUtYmM3YjJlYjU2ODE5?hl=en （THLiu.zip)
#   全字庫:
#       https://www.cns11643.gov.tw/
#

import os
import sys

target_dir = 'THLiu.Tables'
boshiamy_table_txt = 'THLiu.txt' # from https://drive.google.com/drive/folders/0B_9ob1iJjpkLMjA3MDYzM2YtMWNhOS00MTUxLTlhMGUtYmM3YjJlYjU2ODE5?hl=en （THLiu.zip)

# Kanbun                        - 漢文訓讀
# Hiragana                      - 平假名
# Katakana                      - 片假名
# Kangxi Radicals               - 部首
# CJK Radicals Supplement       - 部首
# Halfwidth and Fullwidth Forms - 全型符號

class UniCodeBlock():
    def __init__(self, name, start, end):
        self.name = name
        self.start = start
        self.end = end
        self.table = None
        self.log = None
        self.chars = set()
    def __del__(self):
        if self.table:
            self.table.close()
            self.table = None
        if self.log:
            self.log.close()
            self.log = None

blocks = [
    UniCodeBlock('00 Bopomofo',                                 0x3100,   0x312F),
    UniCodeBlock('00 Bopomofo Extended',                        0x31A0,   0x31BF),
    UniCodeBlock('01 CJK Unified Ideographs',                   0x4E00,   0x9FFF),
    UniCodeBlock('02 CJK Unified Ideographs Extension A',       0x3400,   0x4DBF),
    UniCodeBlock('03 CJK Unified Ideographs Extension B',       0x20000,  0x2A6DF),
    UniCodeBlock('04 CJK Unified Ideographs Extension C',       0x2A700,  0x2B73F),
    UniCodeBlock('05 CJK Unified Ideographs Extension D',       0x2B740,  0x2B81F),
    UniCodeBlock('06 CJK Unified Ideographs Extension E',       0x2B820,  0x2CEAF),
    UniCodeBlock('07 CJK Unified Ideographs Extension F',       0x2CEB0,  0x2EBEF),
    UniCodeBlock('08 CJK Compatibility Ideographs',             0xF900,   0xFAFF),
    UniCodeBlock('09 Kangxi Radicals',                          0x2F00,   0x2FDF),
    UniCodeBlock('10 CJK Radicals Supplement',                  0x2E80,   0x2EFF),
    UniCodeBlock('11 CJK Compatibility Ideographs Supplement',  0x2F800,  0x2FA1F),
    UniCodeBlock('12 CJK Strokes',                              0x31C0,   0x31EF),
    UniCodeBlock('13 CJK Compatibility Forms',                  0xFE30,   0xFE4F),
    UniCodeBlock('14 Halfwidth and Fullwidth Forms',            0xFF00,   0xFFEE),
    UniCodeBlock('30 CJK Symbols and Punctuation',              0x3000,   0x303F),
    UniCodeBlock('32 Enclosed CJK Letters and Months',          0x3200,   0x32FF),
    UniCodeBlock('33 CJK Compatibility',                        0x3300,   0x33FF),
    UniCodeBlock('31 Kanbun',                                   0x3190,   0x319F),
    UniCodeBlock('30 Hiragana',                                 0x3040,   0x309F),
    UniCodeBlock('30 Katakana',                                 0x30A0,   0x30FF),
    UniCodeBlock('X0 Private Use Area',                         0xE000,   0xF8FF),
    UniCodeBlock('X1 Supplementary Private Use Area-A',         0xF0000,  0xFFFFD),
    UniCodeBlock('X2 Supplementary Private Use Area-B',         0x100000, 0x10FFFD),
]
unc_block = \
    UniCodeBlock('X9 Uncategorized',                            0,        0)

import io
import chardet
import codecs
class OPEN():
    """A Unicode-aware wrapper for open(). Note: This is not a fully functional implementation."""
    def __init__(self, file, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True, opener=None):
        self.handle = None
        self.file = file
        self.mode = mode
        self.buffering = buffering
        self.encoding = encoding
        self.errors = errors
        self.newline = newline
        self.closefd = closefd
        self.opener = opener
    def __enter__(self):
        if self.encoding is None:
            raw = open(self.file, 'rb').read(32)
            if raw.startswith(codecs.BOM_UTF8):
                self.encoding = 'utf-8-sig'
            else:
                result = chardet.detect(raw)
                self.encoding = result['encoding']
        self.handle = io.open(self.file, self.mode, self.buffering, self.encoding, self.errors, self.newline, self.closefd, self.opener)
        return self.handle
    def __exit__(self, type, value, traceback):
        if self.handle:
            self.handle.close()
            self.handle = None

if __name__ == '__main__':
    try:
        boshiamy_table_txt = sys.argv[1]
        target_dir = sys.argv[2]
    except IndexError:
        pass

    if not os.path.exists(target_dir):
        os.mkdir(target_dir)

    with OPEN(boshiamy_table_txt, 'r') as fin:
        for c in fin:
            cx = c
            c = c.strip()
            if not c or c[0] in {';', '#'}:
                continue
            try:
                c0, c1 = c.split()
            except ValueError:
                continue
            try:
                ordx = ord(c1)
            except TypeError:
                print(f'Error: [{c}]')
                continue
            
            for b in blocks:
                if b.start <= ordx <= b.end:
                    if not b.table:
                        b.table = open(f'{target_dir}/{b.name}.txt', 'w')
                    b.table.write(f'{c}\n')
                    b.chars.add(ordx)
                    break
            else:
                b = unc_block
                if not b.table:
                    b.table = open(f'{target_dir}/{b.name}.txt', 'w')
                b.table.write(f'{c}\n')
                if not b.log:
                    b.log = open(f'{target_dir}/{b.name}.log', 'w')
                b.log.write(f'{c0} {c1} \t- {hex(ordx)}\n')

    # check and log missing characters in unicode-blocks
    for b in blocks:
        if len(b.chars):
            for r in range(b.start, b.end+1):
                if r not in b.chars:
                    if not b.log:
                        b.log = open(f'{target_dir}/{b.name}.log', 'w')
                    b.log.write (f'{chr(r)} - {hex(r)}\n')
            print(f'Create: {b.name} of range[{b.start:X}..{b.end:X}]')
        else:
            print(f' Empty: {b.name} of range[{b.start:X}..{b.end:X}]')
